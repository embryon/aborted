#ifndef GAME_H
#define GAME_H

#include <list>

#include "Singleton.h"


namespace GameEngine {

class Module;

class Game: public Singleton<Game> {
    friend class Singleton<Game>;

private:
    Game();
    ~Game();

    std::list<Module*> l_modules;

public:
    void init();
    void run();
    void clean();
};

}


#endif // GAME_H
