#ifndef DISPATCHER_H
#define DISPATCHER_H

#include "Singleton.h"


namespace GameEngine {


struct ModuleMessage {
};


class Dispatcher: public Singleton<Dispatcher> {
    friend class Singleton<Dispatcher>;
};

}


#endif // DISPATCHER_H
