#include <SFML/Graphics.hpp>

#include "Game.h"


int main(int argc, const char* argv[]) {
    GameEngine::Game* game = GameEngine::Game::getInstance();

    game->init();
    game->run();
    game->clean();

    return EXIT_SUCCESS;
}
