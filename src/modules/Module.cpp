#include "Module.h"
#include "../Dispatcher.h"

using namespace GameEngine;


Module::Module(Dispatcher* dispatcher) {
    m_pDispatcher = dispatcher;
}


/**
 * Get message from other modules
 */
 void Module::push_message(ModuleMessage& msg) {
    messages_queue.push(msg);
}


/**
 * Process the messages
 */
void Module::process_queue() {
    while (!messages_queue.empty()) {
        ModuleMessage msg = messages_queue.front();
        messages_queue.pop();

        process_event(msg);
    }
}
