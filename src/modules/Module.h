#ifndef MODULE_H
#define MODULE_H

#include <queue>

#include "../Dispatcher.h"
#include "../Singleton.h"


namespace GameEngine {


class Module : public Singleton<Module> {
    friend class Singleton<Module>;

private:
    Module(Dispatcher* dispatcher);
    virtual ~Module() = 0;

    void process_queue();
	virtual void process_event(ModuleMessage& msg) = 0;

    Dispatcher *m_pDispatcher;
    std::queue<ModuleMessage> messages_queue;

public:
    void push_message(ModuleMessage& msg);

    virtual void init() = 0;
    virtual void run() = 0;
    virtual void clean() = 0;
};

}


#endif // MODULE_H
